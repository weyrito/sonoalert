#include <Encoder.h>
#include <LiquidCrystal.h>

// Définition des constantes
const int pinBuzzer = 12, pinCLK = 6, pinDT = 7, pinSW = 8, microphonePin = A0;
const int rs = 11, en = 10, d4 = 5, d5 = 4, d6 = 2, d7 = 3, greenLedPin = 9;
const int buzzerFrequency = 2000, buzzerDuration = 500, numBeeps = 6, beepDuration = 150, pauseDuration = 150;
const String volumeLevels[] = {"LOW", "MEDIUM", "HIGH"};
const String volumeLevelsDisplay[] = {"LOW", "MID", "HIGH"};

// Initialisation des variables pour le contrôle du volume
int volumeThresholds[] = {0, 0, 0};
int volumeLevel = 0;
bool isChoosingVolume = true, volumeChanged = false;

// Initialisation des variables pour le contrôle de la durée
int duration = 0;
bool isChoosingDuration = false, durationChanged = false;

// Initialisation des variables pour le contrôle de l'alarme
bool isPlayingAlarm = false, isRunning = false;
int lastButtonState = HIGH;
unsigned long startTime = 0, alarmStartTime = 0, lastScreenUpdateTime = 0;
unsigned long alarmDuration = beepDuration * numBeeps + pauseDuration * (numBeeps - 1);
int beepCount = 0;

// Initialisation des objets
Encoder myEnc(pinCLK, pinDT);
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void toggleAlarm()
{
  // Gestion de l'alarme
  if (!isPlayingAlarm)
  {
    isPlayingAlarm = true;
    beepCount = 0;
  }

  if (isPlayingAlarm && beepCount < numBeeps)
  {
    tone(pinBuzzer, buzzerFrequency);
    delay(beepDuration);
    noTone(pinBuzzer);
    delay(pauseDuration);
    beepCount++;
  }

  if (beepCount >= numBeeps)
  {
    isPlayingAlarm = false;
    delay(500);
  }
}

void setup()
{
  // Initialisation du port série et des pins
  Serial.begin(9600);
  pinMode(pinSW, INPUT_PULLUP);
  pinMode(greenLedPin, OUTPUT);

  // Initialisation de l'écran LCD
  lcd.begin(16, 2);
  lcd.print("Sampling sound");
  lcd.setCursor(0, 1);
  lcd.print("level...");

  // Échantillonnage du son
  const int numSamples = 500;
  long totalSound = 0;
  for (int i = 0; i < numSamples; i++)
  {
    totalSound += analogRead(microphonePin);
    delay(10);
  }
  int soundSample = totalSound / numSamples;

  // Définition des seuils de volume
  volumeThresholds[0] = soundSample + 5;
  volumeThresholds[1] = soundSample + 10;
  volumeThresholds[2] = soundSample + 15;

  // Affichage du message de choix de volume
  lcd.clear();
  lcd.print("Choose max");
  lcd.setCursor(0, 1);
  lcd.print("volume");
}

void loop()
{
  // Lecture de l'encodeur et du bouton
  long positionChange = myEnc.readAndReset();
  int buttonState = digitalRead(pinSW);

  // Gestion du changement de position de l'encodeur
  if (positionChange != 0)
  {
    if (isChoosingVolume)
    {
      volumeLevel = constrain(volumeLevel - positionChange, 0, 2);
      lcd.clear();
      lcd.print("Volume: " + volumeLevels[volumeLevel]);
      volumeChanged = true;
    }
    else if (isChoosingDuration)
    {
      duration = constrain(duration - 5 * positionChange, 0, 60);
      lcd.clear();
      lcd.print("Duration: " + String(duration) + "min");
      durationChanged = true;
    }
  }

  // Gestion du changement d'état du bouton
  if (buttonState != lastButtonState)
  {
    delay(100);
    lastButtonState = buttonState;

    if (buttonState == LOW)
    {
      if (isChoosingVolume && volumeChanged)
      {
        isChoosingVolume = false;
        isChoosingDuration = true;
        lcd.clear();
        lcd.print("Choose max");
        lcd.setCursor(0, 1);
        lcd.print("duration");
      }
      else if (isChoosingDuration && durationChanged)
      {
        isChoosingDuration = false;
        isRunning = true;
        lcd.clear();
        lcd.print("Timer started");
        digitalWrite(greenLedPin, HIGH);
        startTime = millis();
      } 
      else if (isRunning)
      {
        volumeChanged = false;
        durationChanged = false;
        isRunning = false;
        digitalWrite(greenLedPin, LOW);
        isChoosingVolume = true;
        lcd.clear();
        lcd.print("Choose max");
        lcd.setCursor(0, 1);
        lcd.print("volume");
      }
    }
  }

  // Gestion du timer
  if (isRunning)
  {
    long remainingTime = duration * 60 - (millis() - startTime) / 1000;
    if (remainingTime <= 0)
    {
      isRunning = false;
      digitalWrite(greenLedPin, LOW);
      isChoosingDuration = true;
      lcd.clear();
      lcd.print("Choose max");
      lcd.setCursor(0, 1);
      lcd.print("duration");
      return;
    }

    // Gestion de l'alarme en fonction du volume
    int microphoneValue = analogRead(microphonePin);
    if (microphoneValue > volumeThresholds[volumeLevel] && !isPlayingAlarm)
    {
      toggleAlarm();
    }

    if (isPlayingAlarm && millis() - alarmStartTime >= alarmDuration)
    {
      toggleAlarm();
    }

    // Mise à jour de l'affichage
    if (millis() - lastScreenUpdateTime >= 200)
    {
      lcd.clear();
      lcd.print("Timer: " + String(remainingTime / 60) + ":" + String(remainingTime % 60));
      lcd.setCursor(0, 1);
      lcd.print("Volume: " + String(microphoneValue) + " " + volumeLevelsDisplay[volumeLevel]);
      lastScreenUpdateTime = millis();
    }
  }
}